using System.Threading;
using BucketClient;
using BucketClient.Library.Credentials;
using Xunit;
using Xunit.Abstractions;

namespace BucketClientTest
{
    public class IntegrationTest
    {
        public IntegrationTest(ITestOutputHelper output)
        {
            _output = output;
            var config = ConfigurationLoader.LoadConfiguration("secrets.json");

            //AWS
            var aws = config.aws;
            string awsId = aws.id;
            string awsKey = aws.key;
            string awsRegion = aws.region;
            _awsClient =
                BucketClientFactory.CreateClient(CloudServiceProvider.AWS, new AWSCredential(awsId, awsKey, awsRegion));
            _awsTargetBucket = aws.bucket;

            //DO
            var DO = config.DO;
            string doId = DO.id;
            string doKey = DO.key;
            string doRegion = DO.region;
            _doClient = BucketClientFactory.CreateClient(CloudServiceProvider.DigitalOcean,
                new DigitalOceanCredential(doId, doKey, doRegion));
            _doTargetBucket = DO.bucket;

            //Azure
            var azure = config.azure;
            string azureId = azure.id;
            string azurekey = azure.key;
            _azureClient =
                BucketClientFactory.CreateClient(CloudServiceProvider.Azure, new AzureCredential(azureId, azurekey));
            _azureTargetBucket = azure.bucket;
        }

        private readonly IBucketClient _awsClient;
        private readonly IBucketClient _doClient;
        private readonly IBucketClient _azureClient;
        private readonly string _awsTargetBucket;
        private readonly string _doTargetBucket;
        private readonly string _azureTargetBucket;
        private readonly ITestOutputHelper _output;

        [Trait("TestLevel", "Integration")]
        [Fact]
        public async void AwsCrud()
        {
            var client = _awsClient;
            var buc = _awsTargetBucket;
            var test = 0;
            //Bucket should not exist
            var bucketShouldNotExist1 = await client.ExistBucket(buc);
            Assert.False(bucketShouldNotExist1);
            _output.WriteLine("Passed test" + test++); //0

            //Create bucket
            var resp = await client.CreateBucket(buc);
            Assert.True(resp.Success);
            _output.WriteLine("Passed test" + test++); //1

            //Bucket should ow exist!
            var bucketShouldExist1 = await client.ExistBucket(buc);
            Assert.True(bucketShouldExist1);
            _output.WriteLine("Passed test" + test++); //2

            //Should be able to set public policy
            var publicResp = await client.SetReadPolicy(buc, ReadAccess.Public);
            Assert.True(publicResp.Success);
            _output.WriteLine("Passed test" + test++); //3

            var blob = ConfigurationLoader.LoadBlobAsBytes("sophie.png");
            var bucket = await client.GetBucket(buc);

            //Bucket should not exist
            var blobExist1 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist1);
            _output.WriteLine("Passed test" + test++); //4

            //Should fail to update non-existing blob
            var failUpdate = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.False(failUpdate.Success);
            _output.WriteLine("Passed test" + test++); //5

            //Should successfully put non-existing blob
            var passPut1 = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(passPut1.Success);
            _output.WriteLine("Passed test" + test++); //6

            //Should fail to create exist blob
            var create1 = await bucket.CreateBlob(blob, "sophie.png");
            Assert.False(create1.Success);
            _output.WriteLine("Passed test" + test++); //7

            //Should successfully delete blob using URI
            var del1 = await bucket.DeleteBlob(passPut1.Usable);
            Assert.True(del1.Success);
            _output.WriteLine("Passed test" + test++); //8

            //Should successfully create blob
            var createResp = await bucket.CreateBlob(blob, "sophie.png");
            Assert.True(createResp.Success);
            _output.WriteLine("Passed test" + test++); //9

            //Blob should exist        
            var blobExist2 = await bucket.ExistBlob("sophie.png");
            Assert.True(blobExist2);
            _output.WriteLine("Passed test" + test++); //10

            //Should be able to update blob using uri
            var updateURI = await bucket.UpdateBlob(blob, createResp.Usable);
            Assert.True(updateURI.Success);
            _output.WriteLine("Passed test" + test++); //11

            //should be able to update blob using key
            var updateKey = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.True(updateKey.Success);
            _output.WriteLine("Passed test" + test++); //12

            //Should be able to put blob using uri
            var putURI = await bucket.PutBlob(blob, createResp.Usable);
            Assert.True(putURI.Success);
            _output.WriteLine("Passed test" + test++); //13

            //Should be able to update blob using uri
            var putKey = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(putKey.Success);
            _output.WriteLine("Passed test" + test++); //14

            //Should be able to delete blob using key
            var delBlob = await bucket.DeleteBlob("sophie.png");
            Assert.True(delBlob.Success);
            _output.WriteLine("Passed test" + test++); //15

            //Should not exist with uri
            var blobExist3 = await bucket.ExistBlob(createResp.Usable);
            Assert.False(blobExist3);
            _output.WriteLine("Passed test" + test++); //16

            //Should not exist with key
            var blobExist4 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist4);
            _output.WriteLine("Passed test" + test++); //17

            //Should be able to set public private
            var privateBucket = await client.SetReadPolicy(buc, ReadAccess.Private);
            Assert.True(privateBucket.Success);
            _output.WriteLine("Passed test" + test++, "Private Policy set!"); //18

            //Should be able to delete bucket
            var delBucket = await client.DeleteBucket(buc);
            Assert.True(delBucket.Success);
            _output.WriteLine("Passed test" + test++); //19

            //Bucket should no longer exist
            var existBucket2 = await client.ExistBucket(buc);
            Assert.False(existBucket2);
            _output.WriteLine("Passed test" + test++); //20
        }

        [Trait("TestLevel", "Integration")]
        [Fact]
        public async void AzureCrud()
        {
            var client = _azureClient;
            var buc = _azureTargetBucket;

            _output.WriteLine(client + buc);
            var test = 0;

            //Bucket should not exist
            var bucketShouldNotExist1 = await client.ExistBucket(buc);
            Assert.False(bucketShouldNotExist1);
            _output.WriteLine("Passed test" + test++); //0

            //Create bucket
            var resp = await client.CreateBucket(buc);
            _output.WriteLine(resp.StatusCode.ToString());
            _output.WriteLine(resp.Message);
            Assert.True(resp.Success);
            _output.WriteLine("Passed test" + test++); //1

            Thread.Sleep(5000);

            //Bucket should ow exist!
            var bucketShouldExist1 = await client.ExistBucket(buc);
            Assert.True(bucketShouldExist1);
            _output.WriteLine("Passed test" + test++); //2

            //Should be able to set public policy
            var publicResp = await client.SetReadPolicy(buc, ReadAccess.Public);
            Assert.True(publicResp.Success);
            _output.WriteLine("Passed test" + test++); //3

            var blob = ConfigurationLoader.LoadBlobAsBytes("sophie.png");
            var bucket = await client.GetBucket(buc);

            //Bucket should not exist
            var blobExist1 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist1);
            _output.WriteLine("Passed test" + test++); //4

            //Should fail to update non-existing blob
            var failUpdate = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.False(failUpdate.Success);
            _output.WriteLine("Passed test" + test++); //5

            //Should successfully put non-existing blob
            var passPut1 = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(passPut1.Success);
            _output.WriteLine("Passed test" + test++); //6

            //Should fail to create exist blob
            var create1 = await bucket.CreateBlob(blob, "sophie.png");
            Assert.False(create1.Success);
            _output.WriteLine("Passed test" + test++); //7

            //Should successfully delete blob using URI
            var del1 = await bucket.DeleteBlob(passPut1.Usable);
            Assert.True(del1.Success);
            _output.WriteLine("Passed test" + test++); //8

            Thread.Sleep(5000);

            //Should successfully create blob
            var createResp = await bucket.CreateBlob(blob, "sophie.png");
            Assert.True(createResp.Success);
            _output.WriteLine("Passed test" + test++); //9

            Thread.Sleep(5000);

            //Blob should exist        
            var blobExist2 = await bucket.ExistBlob("sophie.png");
            Assert.True(blobExist2);
            _output.WriteLine("Passed test" + test++); //10

            //Should be able to update blob using uri
            var updateURI = await bucket.UpdateBlob(blob, createResp.Usable);
            Assert.True(updateURI.Success);
            _output.WriteLine("Passed test" + test++); //11

            //should be able to update blob using key
            var updateKey = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.True(updateKey.Success);
            _output.WriteLine("Passed test" + test++); //12

            //Should be able to put blob using uri
            var putURI = await bucket.PutBlob(blob, createResp.Usable);
            Assert.True(putURI.Success);
            _output.WriteLine("Passed test" + test++); //13

            //Should be able to update blob using uri
            var putKey = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(putKey.Success);
            _output.WriteLine("Passed test" + test++); //14

            //Should be able to delete blob using key
            var delBlob = await bucket.DeleteBlob("sophie.png");
            Assert.True(delBlob.Success);
            _output.WriteLine("Passed test" + test++); //15

            Thread.Sleep(5000);

            //Should not exist with uri
            var blobExist3 = await bucket.ExistBlob(createResp.Usable);
            Assert.False(blobExist3);
            _output.WriteLine("Passed test" + test++); //16

            //Should not exist with key
            var blobExist4 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist4);
            _output.WriteLine("Passed test" + test++); //17

            //Should be able to set public private
            var privateBucket = await client.SetReadPolicy(buc, ReadAccess.Private);
            Assert.True(privateBucket.Success);
            _output.WriteLine("Passed test" + test++, "Private Policy set!"); //18

            //Should be able to delete bucket
            var delBucket = await client.DeleteBucket(buc);
            Assert.True(delBucket.Success);
            _output.WriteLine("Passed test" + test++); //19

            Thread.Sleep(10000);

            //Bucket should no longer exist
            var existBucket2 = await client.ExistBucket(buc);
            Assert.False(existBucket2);
            _output.WriteLine("Passed test" + test++); //20
        }

        [Trait("TestLevel", "Integration")]
        [Fact]
        public async void DigitalOceanCrud()
        {
            var client = _doClient;
            var buc = _doTargetBucket;

            _output.WriteLine(client + buc);
            var test = 0;
            //Bucket should not exist
            var bucketShouldNotExist1 = await client.ExistBucket(buc);

            Assert.False(bucketShouldNotExist1);
            _output.WriteLine("Passed test" + test++); //0

            //Create bucket
            var resp = await client.CreateBucket(buc);
            _output.WriteLine(resp.StatusCode.ToString());
            _output.WriteLine(resp.Message);
            Assert.True(resp.Success);
            _output.WriteLine("Passed test" + test++); //1

            //Bucket should ow exist!
            var bucketShouldExist1 = await client.ExistBucket(buc);
            Assert.True(bucketShouldExist1);
            _output.WriteLine("Passed test" + test++); //2

            //Should be able to set public policy
            var publicResp = await client.SetReadPolicy(buc, ReadAccess.Public);
            Assert.True(publicResp.Success);
            _output.WriteLine("Passed test" + test++); //3

            var blob = ConfigurationLoader.LoadBlobAsBytes("sophie.png");
            var bucket = await client.GetBucket(buc);

            //Bucket should not exist
            var blobExist1 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist1);
            _output.WriteLine("Passed test" + test++); //4

            //Should fail to update non-existing blob
            var failUpdate = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.False(failUpdate.Success);
            _output.WriteLine("Passed test" + test++); //5

            //Should successfully put non-existing blob
            var passPut1 = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(passPut1.Success);
            _output.WriteLine("Passed test" + test++); //6

            //Should fail to create exist blob
            var create1 = await bucket.CreateBlob(blob, "sophie.png");
            Assert.False(create1.Success);
            _output.WriteLine("Passed test" + test++); //7

            //Should successfully delete blob using URI
            var del1 = await bucket.DeleteBlob(passPut1.Usable);
            Assert.True(del1.Success);
            _output.WriteLine("Passed test" + test++); //8

            //Should successfully create blob
            var createResp = await bucket.CreateBlob(blob, "sophie.png");
            Assert.True(createResp.Success);
            _output.WriteLine("Passed test" + test++); //9

            //Blob should exist        
            var blobExist2 = await bucket.ExistBlob("sophie.png");
            Assert.True(blobExist2);
            _output.WriteLine("Passed test" + test++); //10

            //Should be able to update blob using uri
            var updateURI = await bucket.UpdateBlob(blob, createResp.Usable);
            Assert.True(updateURI.Success);
            _output.WriteLine("Passed test" + test++); //11

            //should be able to update blob using key
            var updateKey = await bucket.UpdateBlob(blob, "sophie.png");
            Assert.True(updateKey.Success);
            _output.WriteLine("Passed test" + test++); //12

            //Should be able to put blob using uri
            var putURI = await bucket.PutBlob(blob, createResp.Usable);
            Assert.True(putURI.Success);
            _output.WriteLine("Passed test" + test++); //13

            //Should be able to update blob using uri
            var putKey = await bucket.PutBlob(blob, "sophie.png");
            Assert.True(putKey.Success);
            _output.WriteLine("Passed test" + test++); //14

            //Should be able to delete blob using key
            var delBlob = await bucket.DeleteBlob("sophie.png");
            Assert.True(delBlob.Success);
            _output.WriteLine("Passed test" + test++); //15

            //Should not exist with uri
            var blobExist3 = await bucket.ExistBlob(createResp.Usable);
            Assert.False(blobExist3);
            _output.WriteLine("Passed test" + test++); //16

            //Should not exist with key
            var blobExist4 = await bucket.ExistBlob("sophie.png");
            Assert.False(blobExist4);
            _output.WriteLine("Passed test" + test++); //17

            //Should be able to set public private
            var privateBucket = await client.SetReadPolicy(buc, ReadAccess.Private);
            Assert.True(privateBucket.Success);
            _output.WriteLine("Passed test" + test++, "Private Policy set!"); //18

            //Should be able to delete bucket
            var delBucket = await client.DeleteBucket(buc);
            Assert.True(delBucket.Success);
            _output.WriteLine("Passed test" + test++); //19

            //Bucket should no longer exist
            var existBucket2 = await client.ExistBucket(buc);
            Assert.False(existBucket2);
            _output.WriteLine("Passed test" + test++); //20
        }
    }
}