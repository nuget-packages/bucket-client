using BucketClient;
using Xunit;

namespace BucketClientTest
{
    public class MimeTest
    {
        [Fact]
        public void ProperlyDetectGif()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/sophie.gif");
            var mime = image.KirinMime();
            Assert.Equal("image/gif", mime);
        }

        [Fact]
        public void ProperlyDetectJpg()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/sophie.jpg");
            var mime = image.KirinMime();
            Assert.Equal("image/jpeg", mime);
        }

        [Fact]
        public void ProperlyDetectPdf()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/Refactoring.pdf");
            var mime = image.KirinMime();
            Assert.Equal("application/pdf", mime);
        }

        [Fact]
        public void ProperlyDetectPng()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/sophie.png");
            var mime = image.KirinMime();
            Assert.Equal("image/png", mime);
        }

        [Fact]
        public void ProperlyDetectSvg()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/ss.svg");
            var mime = image.KirinMime();
            Assert.Equal("image/svg", mime);
        }

        [Fact]
        public void ProperlyDetectSvgXml()
        {
            var image = ConfigurationLoader.LoadBlobAsBytes("./SampleFiles/sophie.svg");
            var mime = image.KirinMime();
            Assert.Equal("image/svg+xml", mime);
        }
    }
}