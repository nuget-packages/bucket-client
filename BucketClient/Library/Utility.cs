﻿using System.Dynamic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using HeyRed.Mime;
using MimeDetective;
using Newtonsoft.Json;

namespace BucketClient
{
    public static class Utility
    {
        internal static byte[] ToByte(this Stream input)
        {
            using (var ms = new MemoryStream())
            {
                input.CopyTo(ms);
                input.Close();
                return ms.ToArray();
            }
        }

        internal static byte[] ToByteUnsafe(this Stream input)
        {
            using (var ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        internal static Stream ToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes);
        }

        internal static T DeserializeXMLString<T>(this string xmlData)
        {
            var doc = new XmlDocument();
            doc.LoadXml(xmlData);
            var json = JsonConvert.SerializeXmlNode(doc);
            var t = JsonConvert.DeserializeObject<T>(json);
            return t;
        }

        internal static dynamic DeserializeXML(this string xmlData)
        {
            var doc = XDocument.Parse(xmlData); //or XDocument.Load(path)
            var jsonText = JsonConvert.SerializeXNode(doc);
            dynamic dyn = JsonConvert.DeserializeObject<ExpandoObject>(jsonText);
            return dyn;
        }

        public static string KirinMime(this byte[] payload)
        {
            try
            {
                var mime = payload.GetFileType().Mime;
                if (mime == "text/plain") mime = MimeGuesser.GuessMimeType(payload);

                return mime;
            }
            catch
            {
                return "text/plain";
            }
        }

        internal static string GenerateGetCORS(string[] cors)
        {
            return $@"<CORSConfiguration>
                 {string.Join("\n", cors.Select(s => SimpleCORS(s)))}
                </CORSConfiguration>";
        }

        private static string SimpleCORS(string cors)
        {
            return $@"<CORSRule>
                   <AllowedOrigin>{cors}</AllowedOrigin>
                   <AllowedMethod>GET</AllowedMethod>
                   <AllowedHeader>*</AllowedHeader>
                   <MaxAgeSeconds>3000</MaxAgeSeconds>
                 </CORSRule>";
        }
    }
}