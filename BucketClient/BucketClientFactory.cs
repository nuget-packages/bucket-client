﻿using System;
using System.Net.Http;
using BucketClient.AWS;
using BucketClient.Azure;
using BucketClient.DigitalOcean;
using BucketClient.GCP;
using BucketClient.Library.Credentials;

namespace BucketClient
{
    public static class BucketClientFactory
    {
       
        public static IBucketClient CreateClient(CloudServiceProvider service, ICredential credential)
        {
            var httpClient = new HttpClient();
            var aws = credential as AWSCredential;
            var azure = credential as AzureCredential;
            var gcp = credential as GCPCredential;
            var DO = credential as DigitalOceanCredential;
            switch (service)
            {
                case CloudServiceProvider.AWS:
                    if (!(credential is AWSCredential)) throw new ArgumentException("AWS needs AWSCredential!");
                    return new AWSBucketClient(httpClient, aws.accessKeyID, aws.accessKeySecret, aws.region);
                case CloudServiceProvider.Azure:
                    if (!(credential is AzureCredential)) throw new ArgumentException("Azure needs AzureCredential!");
                    return new AzureBucketClient(azure.AccountName, azure.Secret);
                case CloudServiceProvider.GCP:
                    if (!(credential is GCPCredential)) throw new ArgumentException("Google Cloud Platform needs GCPCredential!");
                    return new GCPBucketClient(gcp.projectID, gcp.secretJSON);
                case CloudServiceProvider.DigitalOcean:
                    if (!(credential is DigitalOceanCredential)) throw new ArgumentException("Digital Ocean needs DigitalOceanCredential!");
                    return new DigitalOceanBucketClient(httpClient, DO.accessKeyID, DO.accessKeySecret, DO.region);
                case CloudServiceProvider.AliCloud:
                    throw new NotImplementedException();
                default:
                    return null;
            }
        }
    }

    public enum CloudServiceProvider
    {
        AWS,
        Azure,
        GCP,
        DigitalOcean,
        AliCloud
    }

    public interface ICredential
    {

    } 

    
}
