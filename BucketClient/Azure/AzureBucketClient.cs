﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;

namespace BucketClient.Azure
{
    internal class AzureBucketClient : IBucketClient
    {
        private readonly CloudBlobClient _client;

        public AzureBucketClient(string accountName, string secret)
        {
            var credential = new StorageCredentials(accountName, secret);
            var storage = new CloudStorageAccount(credential, true);
            _client = storage.CreateCloudBlobClient();
        }

        public async Task<byte[]> GetBlob(Uri key)
        {
            var blob = await _client.GetBlobReferenceFromServerAsync(key);
            using (var stream = new MemoryStream())
            {
                await blob.DownloadToStreamAsync(stream);
                return stream.ToArray();
            }
        }

        #region BUCKET

        public async Task<OperationResult> CreateBucket(string key)
        {
            try
            {
                var container = _client.GetContainerReference(key);
                var success = await container.CreateIfNotExistsAsync();
                if (!success) return new OperationResult(false, "Failed to create bucket", HttpStatusCode.BadRequest);
                return new OperationResult(true, "", HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }


        public async Task<OperationResult> DeleteBucket(string key)
        {
            try
            {
                var container = _client.GetContainerReference(key);
                var success = await container.DeleteIfExistsAsync();
                if (!success) return new OperationResult(false, "Failed to delete bucket", HttpStatusCode.BadRequest);
                return new OperationResult(true, "", HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }


        public Task<bool> ExistBucket(string key)
        {
            var container = _client.GetContainerReference(key);
            return container.ExistsAsync();
        }

        public async Task<IBucket> GetBucket(string key)
        {
            var exist = await ExistBucket(key);
            if (!exist) return null;
            var bucket = _client.GetContainerReference(key);
            return new AzureBucket(bucket, _client, this);
        }

        public async Task<OperationResult> SetReadPolicy(string key, ReadAccess access)
        {
            try
            {
                var exist = await ExistBucket(key);
                if (!exist) return new OperationResult(false, "Bucket does not exist", HttpStatusCode.NotFound);
                var bucket = _client.GetContainerReference(key);
                var perm = await bucket.GetPermissionsAsync();
                perm.PublicAccess = access == ReadAccess.Public
                    ? BlobContainerPublicAccessType.Blob
                    : BlobContainerPublicAccessType.Off;
                await bucket.SetPermissionsAsync(perm);
                return new OperationResult(true, "Permission is now " + access, HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }

        public Task<IBucket> UnsafeGetBucket(string key)
        {
            var bucket = _client.GetContainerReference(key);
            return Task.FromResult(new AzureBucket(bucket, _client, this) as IBucket);
        }

        public async Task<OperationResult> SetGETCors(string key, string[] cors)
        {
            try
            {
                var bucket = _client.GetContainerReference(key);
                var properties = await _client.GetServicePropertiesAsync();
                properties.Cors.CorsRules.Clear();
                properties.Cors.CorsRules.Add(
                    new CorsRule
                    {
                        AllowedMethods = CorsHttpMethods.Get,
                        AllowedOrigins = cors.ToList(),
                        AllowedHeaders = new List<string> {"*"}
                    });
                await _client.SetServicePropertiesAsync(properties);
                return new OperationResult(true, "", HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region BLOB

        public async Task<OperationResult> DeleteBlob(Uri key)
        {
            try
            {
                var blob = await _client.GetBlobReferenceFromServerAsync(key);
                var success = await blob.DeleteIfExistsAsync();
                if (success) return new OperationResult(true, "", HttpStatusCode.OK);
                return new OperationResult(false, "", HttpStatusCode.BadRequest);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }

        public async Task<bool> ExistBlob(Uri key)
        {
            try
            {
                var blob = await _client.GetBlobReferenceFromServerAsync(key);
                return await blob.ExistsAsync();
            }
            catch
            {
                return false;
            }
        }

        public async Task<OperationResult> PutBlob(byte[] payload, Uri key)
        {
            try
            {
                var blob = await _client.GetBlobReferenceFromServerAsync(key);
                await blob.UploadFromByteArrayAsync(payload, 0, payload.Length);
                blob.Properties.ContentType = payload.KirinMime();
                await blob.SetPropertiesAsync();
                return new OperationResult(true, "", HttpStatusCode.OK).AppendUri(blob.Uri);
            }
            catch (Exception e)
            {
                return new OperationResult(false, e.Message, HttpStatusCode.BadRequest);
            }
        }

        public Task<OperationResult> PutBlob(Stream payload, Uri key)
        {
            return PutBlob(payload.ToByte(), key);
        }


        public async Task<OperationResult> UpdateBlob(byte[] payload, Uri key)
        {
            var exist = await ExistBlob(key);
            if (!exist) return new OperationResult(false, "Blob does not exist", HttpStatusCode.NotFound);
            return await PutBlob(payload, key);
        }

        public Task<OperationResult> UpdateBlob(Stream payload, Uri key)
        {
            return UpdateBlob(payload.ToByte(), key);
        }

        #endregion
    }
}